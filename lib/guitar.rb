class Guitar
  def initialize (midi_guitar)
    @midi_guitar = midi_guitar
  end
  def play(tab)
    @midi_guitar.play(tab.string_notes)
  end
end

class Tab
  def initialize(tabstr)
    @tabstr = tabstr
  end
  def string_notes
    str = ''
    firsttime = true;
    @tabstr.scan(/\d/).each do |d|
      str += (firsttime ? '': ' ') + "B#{d}"
      firsttime = false
    end
    return str
  end
end