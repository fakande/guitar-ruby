require 'cucumber/rspec/doubles'
require 'guitar'
require 'midi_guitar'

Given(/^I start my application$/) do
  @midi_guitar = spy(MidiGuitar)
  @guitar = Guitar.new(@midi_guitar)
end

Given(/^I load the following guitar tab$/) do |tabstr|
  @tab = Tab.new(tabstr)
end

When(/^the guitar plays$/) do
  @guitar.play(@tab)
end

Then(/^the following notes should be played$/) do |tabstr|
  expect(@midi_guitar).to have_received(:play).with(tabstr)
end